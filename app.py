from flask import Flask, jsonify, request, Blueprint
from JsonUtil import jsonEncoder
from JsonUtil.JsonSchema import user_schema
from flask_pymongo import PyMongo
from Data.mognoHandler import mongoHandler
from flask_jwt_extended import JWTManager
from flask_bcrypt import Bcrypt
from router import report
import config
import json
import os
import socket
import logging
import logger
import ast
import datetime

logdir = os.path.join(os.curdir, config.LOG_CONFIG['logPath'])
logger.init(logdir, config.LOG_CONFIG['fileName'])

logging.info("Creating Flask app...")

app = Flask(__name__)
app.env = config.default['environment']
app.config['MONGO_DBNAME'] = config.MONGO_DB['AppMongoName']
app.config['MONGO_URI'] = "".join((config.MONGO_DB['mongoConnString'], config.MONGO_DB['AppMongoName'],
                                   '?retryWrites=', str(config.MONGO_DB['mongoRetryWrites']).lower()))

app.config['JWT_SECRET_KEY'] = os.environ.get('SECRET')
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1)

logging.info("Creating Flask_pymongo app...")
directMongo = PyMongo(app)
mongoApp = mongoHandler(directMongo)
ReportsCollectionName = config.MONGO_DB['ReportsCollectionName']

flask_bcrypt = Bcrypt(app)
jwt = JWTManager(app)
app.json_encoder = jsonEncoder.JSONEncoder

report.config['config'] = config
report.config['mongoApp'] = mongoApp
app.register_blueprint(report.reports, url_prefix="/report")


@app.route('/', methods=['GET'])
def getEnv():
    logging.debug("starting getEnv endpoint")
    return jsonify({
        'host_name': config.default['HOST_NAME'],
        'app_name': config.default['APP_NAME'],
        'ip': config.default['IP'],
        'port': config.default['PORT'],
        'home_dir': config.default['HOME_DIR'],
        'host': socket.gethostname()
    })


hostname = socket.gethostname()
ip = socket.gethostbyname(socket.gethostname())


@app.route('/login/register', methods=['POST'])
def register():
    ''' register user endpoint '''
    data = user_schema.validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        # TODO: use salt!
        data['password'] = flask_bcrypt.generate_password_hash(
            data['password'])
        mongoApp.insert('users', data)
        return jsonify({'ok': True, 'message': 'User created successfully!'}), 200
    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/login/auth', methods=['POST'])
def auth_user():
    ''' auth endpoint '''
    data = user_schema.validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        user = mongoApp.find_one("users", {'email': data['email']})#, "_id": 0
        if user and flask_bcrypt.check_password_hash(user['password'], data['password']):
            del user['password']
            access_token = jwt._create_access_token(identity=data)
            refresh_token = jwt._create_refresh_token(identity=data)
            user['token'] = access_token
            user['refresh'] = refresh_token
            return jsonify({'ok': True, 'data': user}), 200
        else:
            return jsonify({'ok': False, 'message': 'invalid username or password'}), 401
    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


if __name__ == '__main__':
    app.run(host=config.default['IP'], port=config.default['PORT'])
