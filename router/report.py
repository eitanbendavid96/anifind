from flask import Blueprint, request
import logging
import ast

config = {}

reports = Blueprint('report', __name__)

@reports.route('/', methods=['GET'])
def get_all_reports():
    logging.debug("starting getAllReports endpoint")
    return config['mongoApp'].getAllCollection(config['config'].MONGO_DB['ReportsCollectionName'])


@reports.route('/filter', methods=['GET'])
def get_filterd():
    logging.debug("starting getClosestReports endpoint")
    cordinate = ast.literal_eval(request.args.get('Cordinate'))
    maxResult = int(request.args.get('maxResult'))
    return config['mongoApp'].getClosestResult(config['config'].MONGO_DB['ReportsCollectionName'], cordinate, maxResult)


@reports.route('/<reportId>', methods=['GET'])
def get_one_report(reportId):
    logging.debug("starting getOneReport endpoint")
    logging.debug("report id:" + reportId)
    return config['mongoApp'].getReportByObjId(config['config'].MONGO_DB['ReportsCollectionName'], reportId)


@reports.route('/', methods=['POST'])
def add_report():
    logging.debug("starting insertingOneReport endpoint")
    location = request.json['loc']
    logging.debug("report location:" + str(location))
    return config['mongoApp'].insertReport(config['config'].MONGO_DB['ReportsCollectionName'], location)
