from bson import ObjectId
import logging
import json

class JSONEncoder(json.JSONEncoder): 
    def default(self, o): # pylint: disable=E0202
        if isinstance(o, ObjectId):
            logging.debug("the json to encode is ObjectId instance. parsing to str...")
            return str(o)
        return json.JSONEncoder.default(self, o)