from JsonUtil.jsonEncoder import JSONEncoder
from flask_pymongo import pymongo
import logging

class Build:
    def reportResult(self, reports):
        logging.info("Parsing reports to Array of Jsons")

        if (not isinstance(reports, list) and not isinstance(reports, pymongo.cursor.Cursor)):
            logging.warn(
                "object to parse is not list or cursor. wrapping obj with array...")
            reports = [reports]

        output = []
        for report in reports:
            logging.debug("about to append report: " + str(report))
            output.append({"id": report["_id"], "location": report["loc"]})

        logging.info("output to return: " + str(output))
        resultJson = {"result": output}
        return JSONEncoder().encode(resultJson)

    def Error(self, errorMsg):
        logging.info("building an error json result...")
        return JSONEncoder().encode({'result': {'Error': errorMsg}})
