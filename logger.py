import os
import logging

def init(logDir, fileName ):
    logfile = os.path.join(logDir, fileName)

    if not os.path.exists(logDir):
        os.makedirs(logDir)

    logging.basicConfig(filename=logfile, level=logging.DEBUG)
    logging.getLogger().addHandler(logging.StreamHandler())