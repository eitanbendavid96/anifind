from JsonUtil import jsonResultHelper
from bson import ObjectId
import logging

class mongoHandler:
    def __init__(self, pymongo):
        self.mognoApp = pymongo

    def getAllCollection(self, collectionName):
        reports = self.mognoApp.db[collectionName].find()
        return jsonResultHelper.Build.reportResult(self, reports)

    def getReportByObjId(self, collectionName, reportObjId):
        if not isinstance(reportObjId, ObjectId):
            reportObjId = ObjectId(reportObjId)

        report = self.mognoApp.db[collectionName].find_one(reportObjId)

        if report:
            return jsonResultHelper.Build.reportResult(self, [report])
        else:
            return jsonResultHelper.Build.Error(self, "No such report")

    def getClosestResult(self, collectionName, cordinate, maxResult):
        logging.info(" ".join(["getting closest result from collection:",
                               collectionName, " to cordinate:", str(cordinate)]))

        reports = self.mognoApp.db[collectionName]
        closestReports = reports.find(
            {"loc": {"$near": cordinate}}).limit(maxResult)

        return jsonResultHelper.Build.reportResult(self, closestReports)

    def insertReport(self, collectionName, location):
        logging.info(" ".join(["inserting new report to",
                               collectionName, "location:", str(location)]))

        reports = self.mognoApp.db[collectionName]
        insertedReport = reports.insert_one({'loc': location})

        return self.getReportByObjId(collectionName, insertedReport.inserted_id)
        
    def insert(self, collectionName, data):
        try:
            collection = self.mognoApp.db[collectionName]
            logging.info(" ".join(["inserting data to", collectionName, "data:", str(data)]))
            insertedData = collection.insert_one(data)
            return insertedData
        except Exception as e:
            raise e

    def find_one(self, collectionName, pattern):
            collection = self.mognoApp.db[collectionName]
            result = collection.find_one(pattern)
            return result

    def find_many(self, collectionName, pattern):
            collection = self.mognoApp.db[collectionName]
            result = collection.find(pattern)
            return result



