import os
import socket

Environment = os.environ.get('environment', 'dev')

HOST_NAME = os.environ.get('OPENSHIFT_APP_DNS', 'localhost')
APP_NAME = os.environ.get('OPENSHIFT_APP_NAME', 'anifind')
IP = os.environ.get('Anifind_IP', socket.gethostbyname(socket.gethostname()))
PORT = int(os.environ.get('OPENSHIFT_PYTHON_PORT', 8080))
HOME_DIR = os.environ.get('OPENSHIFT_HOMEDIR', os.getcwd())

mongoConnString = os.environ['AnifindMongoConnStr']
AppMongoName = 'anifind'
ReportsCollectionName = 'reports'
mongoRetryWrites = True

default = {'HOST_NAME': HOST_NAME,
           'APP_NAME': APP_NAME,
           'IP': IP,
           'PORT': PORT,
           'HOME_DIR': HOME_DIR,
           'environment': Environment}

MONGO_DB = {'mongoConnString': mongoConnString,
            'AppMongoName': AppMongoName,
            'ReportsCollectionName': ReportsCollectionName,
            'mongoRetryWrites': mongoRetryWrites}

LOG_CONFIG = {'logPath': 'log',
              'fileName': 'app.log'}
